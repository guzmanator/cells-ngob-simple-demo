# Cells simple demo app
## 1. crear una aplicacion
nos movemos a nuestro directorio de proyectos 
-
~~~ 
cd proyectos

~~~ 

verificamos la version de cells (debe de ser almenos la 0.7.1)
-
~~~ 
~/proyectos$ cells --version
0.7.1
~~~ 

ejecutamos el comando cells para crear una nueva aplicacion
-
~~~ 
cells
~~~ 

elegimos app:create para crearla de cero 
-
~~~ 
  environment:setup (Configure and checks all environment requirements for Cells-cli) 
❯ app:create (Context creation flow) 
  component:create (Context creation flow) 
  cordova-plugin:create [install & execute]  (Context creation flow) 
  workspace:create [install & execute]  (Context creation flow) 
  Get help 
  Exit 
~~~ 

le damos un nombre 
-
~~~ 
cells-ngob-simple-demo 
~~~ 

la aplicacion se crea correctamente 
-
~~~ 
 Finished | Create a Cells application from a scaffold template - 08 s 453 ms 

[13:00:15] Flow [ create ] finished - 08 s 624 ms
[13:00:15] Total time - 10 s 040 ms
~~~ 

cambiamos al directorio de la aplicacion creada
-
~~~ 
cd cells-ngob-simple-demo 
~~~ 

actualizamos las dependencias 
-
~~~ 
bower install
~~~ 

ejecutamos la aplicacion 
-
~~~ 
cells
~~~ 

elegimos la opcion app:serve-app
~~~ 
? What's up!? -> This is "app" 
  app:clean (Clean the distribution of a Cells application) 
  app:publish [install & execute]  (Flow to publish) 
  app:build (Build the distribution of a Cells application) 
❯ app:serve-app (Serve the distribution of a Cells application) 
  app:check-mocks [install & execute]  (Check composer mocks) 
  app:cordova-build (Build the distribution in the folder ./www of a Cordova App) 
  Get help 
  Exit 
~~~ 

elegimos modo no dist 
-
~~~ 
? Choose the distribution type (Use arrow keys)
❯ serve no dist 
  serve dist no vulcanize 
  serve dist vulcanize
~~~ 

elegimos el fichero de configuracion por defecto 
-
~~~ 
? Choose the configuration for the distribution (Use arrow keys)
❯ app/config/env.json 
~~~ 

el comando de serve termina correctamente deberias de ver algo como 
-
~~~ 
[BS] Access URLs:
 --------------------------------------
       Local: http://localhost:5000
    External: http://10.253.24.216:5000
 --------------------------------------
          UI: http://localhost:3001
 UI External: http://10.253.24.216:3001
 --------------------------------------
[BS] Serving files from: .tmp
[BS] Serving files from: app
[HPM] Proxy created: /ASO  ->  http://localhost:8000
[HPM] Proxy created: /DFAUTH  ->  http://localhost:8000
[HPM] Proxy created: /ENPP  ->  http://localhost:8000
[13:04:03] Using gulpfile ~/.nvm/versions/node/v4.2.6/lib/node_modules/cells-app/node_modules/cells-app-gulp-tasks/gulpfile.js
[13:04:04] Starting 'watch'...
[13:04:04] Finished 'watch' after 277 ms
~~~ 


se abre el navegador y vemos una aplicacion con varios componentes, varias paginas y navegacion 
-
~~~ 
http://localhost:5000
~~~ 

## subir  la aplicacion a git 
creamos en bitbucket un repositorio 
-
~~~ 
*ir a bitbucket 

*repository / create a repository 

*darle nombre cells-ngob-simple-demo

*desmarcar privado si aplica 

*pulsar boton crear 
~~~ 


cambiar al directorio donde tienes el codigo 
-
~~~ 
cd /path/to/your/project
~~~
*indicar que quieres un proyecto git 
-
~~~ 
git init
~~~
*en la pagina de despues de crear el repo copiar la ruta para aÃƒÆ’Ã‚Â±adir al repo (algo como esto )*
-
~~~ 
 git remote add origin https://guzmanator@bitbucket.org/guzmanator/cells-ngob-simple-demo.git
~~~

comprobar que va a subir las cosas 
-
~~~ 
       git status
~~~
incluir los ficheros al git 
-
~~~ 
       git add .
~~~
hace run comit para decirle a git que quieres los cambios
-
~~~ 
    git commit -m "Initial commit"
~~~

subir los cambios al repositorio 
-
~~~ 
    git push -u origin master
~~~

crear una tag 
-
~~~ 
git tag -a 0.1.0 -m "init version 0.1.0" 
~~~

subirla al repo 
-
~~~ 
git push origin 0.1.0
~~~

## 2. incluir un componente del repositorio 

incluir el componente en el bower json 
-
~~~ 
    "cells-tabs": "ssh://git@globaldevtools.bbva.com:7999/bgcm/cells-tabs.git#1.1.2",
~~~ 

poner en el home.json la definicion del componente 
-
~~~ 
{
      "zone": "app__main",
      "type":"UI",
      "tag": "cells-tabs",
      "properties": {
        "tabSelected": "movements",
        "cellsConnections": {
          "in": {
            "set_tabs": {
              "bind": "items"
            }
          },
          "out": {
          }
        }
      }
    },
~~~ 

ponerle una propiedad con los datos que necesita 
-
~~~ 
{
...
      "properties": {
        "tabSelected": "movements",
        "items":[
            {
              "id":"resume",
              "key":"resume"
            },
            {
              "id":"movements",
              "key":"movements"
            },
            {
              "id":"fee-less",
              "key":"month-fee-less"
            },
            {
              "id":"fee",
              "key":"month-fee"
            },
            {
              "id":"expenses",
              "key":"analysis-expenses"
            },
            {
              "id":"account",
              "key":"status-account"
            }
          ],
       ...
      }
    },
~~~ 

ejecutamos la aplicacion 
-
~~~ 
cells
~~~ 

elegimos la opcion app:serve-app
-
~~~ 
? What's up!? -> This is "app" 
  app:clean (Clean the distribution of a Cells application) 
  app:publish [install & execute]  (Flow to publish) 
  app:build (Build the distribution of a Cells application) 
❯ app:serve-app (Serve the distribution of a Cells application) 
  app:check-mocks [install & execute]  (Check composer mocks) 
  app:cordova-build (Build the distribution in the folder ./www of a Cordova App) 
  Get help 
  Exit 
~~~ 

elegimos modo no dist 
-
~~~ 
? Choose the distribution type (Use arrow keys)
❯ serve no dist 
  serve dist no vulcanize 
  serve dist vulcanize
~~~ 

elegimos el fichero de configuracion por defecto 
-
~~~ 
? Choose the configuration for the distribution (Use arrow keys)
❯ app/config/env.json 
~~~ 

el comando de serve termina correctamente deberias de ver algo como 
-
~~~ 
[14:44:14] Starting 'serve'...
[BS] Access URLs:
 --------------------------------------
       Local: http://localhost:5000
    External: http://10.253.24.216:5000
 --------------------------------------
          UI: http://localhost:3001
 UI External: http://10.253.24.216:3001
 --------------------------------------
[BS] Serving files from: .tmp
[BS] Serving files from: app
[HPM] Proxy created: /ASO  ->  http://localhost:8000
[HPM] Proxy created: /DFAUTH  ->  http://localhost:8000
[HPM] Proxy created: /ENPP  ->  http://localhost:8000
[14:44:16] Using gulpfile ~/.nvm/versions/node/v4.2.6/lib/node_modules/cells-app/node_modules/cells-app-gulp-tasks/gulpfile.js
[14:44:16] Starting 'watch'...
[14:44:16] Finished 'watch' after 296 ms
~~~ 

##guardar los cambios en git 

modificar la version de la aplicacion en el bower json 
-
~~~ 
  "name": "cells-ngob-simple-demo ",
  "version": "0.1.1",
~~~ 

comprobar que va a subir las cosas 
-
~~~ 
       git status
~~~
incluir los ficheros al git 
-
~~~ 
       git add .
~~~
hace run comit para decirle a git que quieres los cambios
-
~~~ 
    git commit -m "added tabs component in mock mode"
~~~

subir los cambios al repositorio 
-
~~~ 
    git push -u origin master
~~~

crear una tag 
-
~~~ 
git tag -a 0.1.1 -m "added a component from bitbucket version  0.1.1" 
~~~

subirla al repo 
-
~~~ 
git push origin 0.1.1
~~~

## 3. incluir un datamanager

incluir el componente en el bower json 
-
~~~ 
    "datamanagers-mx-new": "ssh://git@globaldevtools.bbva.com:7999/bbvacellsapi/datamanagers-mx-new.git#0.8.20",
~~~ 

eliminar del home.json los "items" de modo mock
-
~~~ 
 {
      "zone": "app__main",
      "type":"UI",
      "tag": "cells-tabs",
      "properties": {
        "tabSelected": "movements",
        "cellsConnections": {
          "in": {
            "set_tabs": {
              "bind": "items"
            }
          },
          "out": {
          }
        }
      }
    },
~~~ 

poner en el home.json la definicion del componente 
-
~~~ 
{
      "zone": "app__main",
      "type": "DM",
      "familyPath": "../../components/datamanagers-mx-new",
      "tag": "dm-tabs",
      "properties": {
        "environment": "",
        "cellsConnections": {
          "in": {},
          "out": {
            "set_tabs": {
              "bind": "tabs-changed"
            }
          }
        }
      }
    }
~~~ 

ejecutar la aplicacion y debera de verse como tabs
-
~~~ 
movimientos    Compras a meses     Estado de cuenta 
~~~ 


##guardar los cambios en git 

modificar la version de la aplicacion en el bower json 
-
~~~ 
  "name": "cells-ngob-simple-demo ",
  "version": "0.1.2",
~~~ 

comprobar que va a subir las cosas 
-
~~~ 
       git status
~~~
incluir los ficheros al git 
-
~~~ 
       git add .
~~~
hace run comit para decirle a git que quieres los cambios
-
~~~ 
    git commit -m "added a datamanager for tabs"
~~~

subir los cambios al repositorio 
-
~~~ 
    git push -u origin master
~~~

crear una tag 
-
~~~ 
git tag -a 0.1.2 -m "added a datamanager for tabs version  0.1.2" 
~~~

subirla al repo 
-
~~~ 
git push origin 0.1.2
~~~
